document.getElementById('signButton').addEventListener('click', () => {
    const code = document.getElementById('codeInput').value;
    const message = document.getElementById('message');

    // Vérifie que le code contient exactement 5 chiffres
    if (/^\d{5}$/.test(code)) {
        message.textContent = "Code validé et signé avec succès!";
        message.style.color = 'green';
    } else {
        message.textContent = 'Le code doit contenir exactement 5 chiffres.';
        message.style.color = 'red';
    }
});